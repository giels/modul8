import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

class FileReading {
	public static void main(String[]args) {
		String line = "", fileContent = "";
		try {
			//BufferedReader fileInput = new BufferedReader(new FileReader(new File("D:\baca.txt")));
			
			FileReader fr = new FileReader("D:/crot.txt");
            BufferedReader fileInput = new BufferedReader(fr);
			
			
			line = fileInput.readLine();
			fileContent = line + "\n";
			while(line !=null) {
				line = fileInput.readLine();
				if(line != null) fileContent += line +"\n";
			}
			fileInput.close();
		}catch(EOFException eofe) {
			System.out.println("No More Line To Read");
			System.exit(0);
		}catch(IOException ioe) {
			System.out.println("Error reading file.");
			System.exit(0);
		}
		System.out.println(fileContent);
	}
}
