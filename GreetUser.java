import java.io.*;

public class GreetUser {
	public static void main(String[]args) throws IOException {
		System.out.println("Hi, what's your name?");
		String name;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		name = br.readLine();
		System.out.println("Nice to meet you, "+name+ "! :)");
		
	}
}
