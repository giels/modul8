import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class WriteFile {

	public static void main(String[] args) throws IOException{
		System.out.println("What is the name of the file to be written to?");
		String fileName;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		fileName = br.readLine();
		System.out.println("Enter data to written to "+fileName);
		System.out.println("Type q$ to end");
		FileOutputStream fos;
		
		try {
			fos = new FileOutputStream(fileName);
			
			boolean done = false;
			int data;
			do {
				data = br.read();
				if((char) data == 'q') {
					data = br.read();
					if((char) data == '$') {
						done = true;
					} else {
						fos.write('q');
						fos.write(data);
					}
				} else {
					fos.write(data);
				}
			} while(!done);
		} catch(FileNotFoundException e) {
			System.out.println("File cannot be opened for writing");
		} catch(IOException e) {
			System.out.println("Problem in reading from the file");
		}
	}

}
